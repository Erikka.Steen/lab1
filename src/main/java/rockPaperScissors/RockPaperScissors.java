package rockPaperScissors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();

    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public void run() {
        System.out.println("Let's play round "+roundCounter);
        
        while(true) {
            String userInput = readInput("Your choice (Rock/Paper/Scissors)?");
            
            //Random tallene for DataValget
            Random valg = new Random();
            int tilfeldigTallNulltilTo = valg.nextInt(3);
            String GetPosisjonenTilRpsChoicesTilOrdet = this.rpsChoices.get(tilfeldigTallNulltilTo);
        
            if(GetPosisjonenTilRpsChoicesTilOrdet.equals(userInput)) {
                System.out.println("Human chose "+ userInput + ", computer chose "+ GetPosisjonenTilRpsChoicesTilOrdet+". It's a tie!");
                System.out.println("Score: human "+humanScore+", computer "+computerScore);
                roundCounter++;

            }
            else if(userInput.equals("rock")&& GetPosisjonenTilRpsChoicesTilOrdet.equals("scissors") || (userInput.equals("paper")&& GetPosisjonenTilRpsChoicesTilOrdet.equals("rock")) || (userInput.equals("scissors")&& GetPosisjonenTilRpsChoicesTilOrdet.equals("paper"))) {
                System.out.println("Human chose "+ userInput+ ", computer chose "+ GetPosisjonenTilRpsChoicesTilOrdet + ". Human wins!");
                humanScore++;
                System.out.println("Score: human "+humanScore+", computer "+computerScore);
                roundCounter++;

              
            }
            else if(GetPosisjonenTilRpsChoicesTilOrdet.equals("rock")&& userInput.equals("scissors") || (GetPosisjonenTilRpsChoicesTilOrdet.equals("paper")&& userInput.equals("rock")) || (GetPosisjonenTilRpsChoicesTilOrdet.equals("scissors")&& userInput.equals("paper"))) {
                System.out.println("Human chose "+ userInput+ ", computer chose "+ GetPosisjonenTilRpsChoicesTilOrdet + ". Computer wins!");
                computerScore++;
                System.out.println("Score: human "+humanScore+", computer "+computerScore);
                roundCounter++;

            
            }
            else {
                System.out.println("I do not understand "+userInput+". Could you try again?");
            }
            
        
            String continueOrNot = readInput("Do you wish to continue playing? (y/n)?");
            if(continueOrNot.equals("n")) {
                System.out.print("Bye bye :)");
                break;
            }
            else {
                System.out.println("Let's play round "+roundCounter);
            }

            

        }
    }
   
    //control C for å stoppe
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
